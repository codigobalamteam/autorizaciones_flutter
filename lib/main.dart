import 'dart:developer';

import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:qr_bar_code_flutter/src/config_app/login_page.dart';
import 'package:qr_bar_code_flutter/src/models/notificacion_model.dart';
import 'package:qr_bar_code_flutter/src/provider/push_notification_provider.dart';
import 'package:qr_bar_code_flutter/src/provider/session_provider.dart';
import 'package:qr_bar_code_flutter/src/routes/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final session = new SessionProvider();
  await session.initPrefs();

  WidgetsFlutterBinding.ensureInitialized();
  await PushNotificationService.initializeApp();

  try {
    String token = (PushNotificationService.token)!;
    session.tokenFirebase = token;
  } catch (e) {}

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  final GlobalKey<ScaffoldMessengerState> messagerKey =
      new GlobalKey<ScaffoldMessengerState>();
  late FlutterLocalNotificationsPlugin localNotification;
  List<String> idNotification = [];

  @override
  void initState() {
    super.initState();

    var androidInitialize =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iosInitialize = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: androidInitialize, iOS: iosInitialize);
    localNotification = new FlutterLocalNotificationsPlugin();
    localNotification.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    PushNotificationService.messagesStream.listen((message) {
      log("Message Notifications: $message");

      setState(() {
        idNotification = message;
      });
      //log("ENTRO AQUI");
      _showNotifications();
    });

    navigatorKey.currentState?.pushNamed('jefes',
        arguments: NotificacionModel(idNotification[0], idNotification[1]));

    // NOTIFICATION ON BACKGROUND HANDLER
/*
    PushNotificationService.messagesStreamBackground.listen((message) {
      log("Message Notifications: $message");
      log("ENTRO ACA");
      setState(() {
        idNotification = message;
      });
      navigatorKey.currentState?.pushNamed('received',
          arguments: NotificacionModel(idNotification[0], idNotification[1]));

      setState(() {
        idNotification = [];
      });
    });*/
  }

  final SessionProvider prov = new SessionProvider();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      systemNavigationBarColor: Colors.grey[900],
      statusBarIconBrightness: Brightness.light,
      //statusBarBrightness: Brightness.dark,
      //systemNavigationBarDividerColor: Colors.pink,
      systemNavigationBarIconBrightness: Brightness.light,
    ));

    String _initial = (prov.authToken != "") ? 'jefes' : 'login';
    //String _initial = (false) ? 'home' : 'login';
    //String _initial = _selectInitialPage(prov.usuario!);

    return MaterialApp(
      title: 'Autorizaciones',
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        //AppLocalizations.delegate, // remove the comment for this line
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,

        GlobalWidgetsLocalizations.delegate,

        //
      ],
      supportedLocales: [const Locale('es', 'ES')],
      theme: FlexThemeData.light(
          scheme: FlexScheme.blueWhale, fontFamily: 'Trade-Gothic'),
      home: LoginPage(),
      initialRoute: _initial,
      //  initialRoute: 'recepcion',
      routes: getApplicationRoutes(),
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
            builder: (BuildContext context) => LoginPage());
      },
    );
  }

  Future _showNotifications() async {
    var androidDetails = new AndroidNotificationDetails(
        'channelId', 'Local Notification', 'This is the description of message',
        importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationsDetails =
        new NotificationDetails(android: androidDetails, iOS: iosDetails);
    await localNotification.show(
        0, idNotification[0], idNotification[1], generalNotificationsDetails);
  }

  Future onSelectNotification(String? payload) async {
    this.build(context);
    navigatorKey.currentState!.pushNamed('jefes',
        arguments: NotificacionModel(idNotification[0], idNotification[1]));
    setState(() {
      idNotification = [];
    });

    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
  }
}

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
