import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_bar_code_flutter/src/pages/close_session_page.dart';

class NavBarCustom extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(60);

  final String title;
  final bool home;
  NavBarCustom(this.title, this.home);

  @override
  Widget build(BuildContext context) {
    String imagenAppBar = "assets/logoRtorres.png";

    return AppBar(
      title: Container(
        child: Text(
          this.title,
          style: TextStyle(color: Colors.white),
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white, //change your color here
      ),
      centerTitle: true,
      actions: <Widget>[
        home
            ? Padding(
                padding:
                    const EdgeInsets.only(right: 20.0, bottom: 10, top: 10),
                child: IconButton(
                  color: Colors.white,
                  icon: Icon(Icons.login_sharp, size: 30),
                  onPressed: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CloseSession()));
                  },
                ),
              )
            : Center()
      ],
      flexibleSpace: Container(
          child: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(0.0),
            bottomLeft: Radius.circular(25.0),
            bottomRight: Radius.circular(25.0),
            topRight: Radius.circular(0.0)),
        child: Container(
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: /*Image.asset(
              imagenAppBar,
              fit: BoxFit.contain,
            ),*/
                Center(),
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.blueAccent, Color.fromRGBO(9, 31, 146, 1)],
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
            ),
          ),
        ),
      )),
      elevation: 0,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(16))),
    );
  }
}
