import 'package:flutter/material.dart';

class textWithIcon extends StatelessWidget {
  final String title;
  final String text;
  final IconData icon;
  final int numberColumns;
  final bool color;
  textWithIcon(this.title, this.text, this.icon,this.numberColumns,this.color);

  @override
  Widget build(BuildContext context) {

    switch(numberColumns){
      case 1:
      return Column(
              children: [
                Row(
                  children: [
                    Icon(icon, color: color ? Colors.blue :Colors.blue , size: 9),
                    Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Text(
                        title,
                        style: TextStyle(
                            color:  color ? Colors.blue :Colors.blue ,
                            fontWeight: FontWeight.bold,
                            fontSize: 10),
                        // softWrap: false,
                        overflow: TextOverflow.clip,
                          
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  child: SelectableText(
                      text,
                      style: TextStyle(
                          color: color ? Colors.black : Colors.white,
                          /*fontWeight: FontWeight.bold,*/ fontSize: 12),
                      // softWrap: false,                    
                      textAlign: TextAlign.left,
                      maxLines: 20,
                  ),
                ), 
              ],
        );

        default: 
        return Flexible(
        child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Icon(icon, color: color ? Colors.white : Colors.blue, size: 9),
                    Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Text(
                        title,
                        style: TextStyle(
                            color: color ? Colors.white :  Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 10),
                        // softWrap: false,
                        overflow: TextOverflow.clip,
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  child: Text(
                      text,
                      style: TextStyle(
                          color: color ? Colors.white : Colors.black ,
                          /*fontWeight: FontWeight.bold,*/ fontSize: 13),
                      // softWrap: false,                    
                      textAlign: TextAlign.left,
                      maxLines: 20,
                  ),
                ), 
              ],
            )
          ),
        );
    }

  }
}
