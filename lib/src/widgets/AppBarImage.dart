import 'package:flutter/material.dart';
import 'package:qr_bar_code_flutter/src/pages/close_session_page.dart';


class AppBarImage extends StatelessWidget {


  String imagenAppBar = "assets/maserplogo.png";

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      title: Text(
        "",
        style: TextStyle(fontSize: 15),
      ),
     /* actions: [
        IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CloseSession()));
          },
          icon: Icon(Icons.login)),
      ],*/
      flexibleSpace: Container(
        
          child: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(0.0),
            bottomLeft: Radius.circular(60.0),
            bottomRight: Radius.circular(60.0),
            topRight: Radius.circular(0.0)),
        child: ColorFiltered(
          colorFilter: ColorFilter.mode(Colors.transparent, BlendMode.darken),
          child: Container(
            width: double.infinity,
            child:
            Padding(
              padding: const EdgeInsets.only(top:40.0,bottom: 15.0 ,right: 140,left: 140),
              child: Image.asset(
                imagenAppBar,
                fit: BoxFit.fitHeight,
                
              ),
            ),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.blueAccent,Color.fromRGBO(9,31, 146, 1) ],
                begin: Alignment.bottomRight,
                end: Alignment.topLeft,
              ),
            ),
          ),
        ),
      )),
      elevation: 20,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(180))),
    );
  }
}