import 'dart:convert';
import 'dart:core';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:qr_bar_code_flutter/src/models/detalle_model.dart';
import 'package:qr_bar_code_flutter/src/models/empresa_model.dart';
import 'package:qr_bar_code_flutter/src/models/login_model.dart';
import 'package:qr_bar_code_flutter/src/models/movimiento_model.dart';
import 'package:qr_bar_code_flutter/src/models/okRef_model.dart';
import 'package:qr_bar_code_flutter/src/models/response_model.dart';
import 'package:qr_bar_code_flutter/src/models/rol_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:qr_bar_code_flutter/src/models/session_model.dart';
import 'package:qr_bar_code_flutter/src/models/uen_model.dart';
import 'package:qr_bar_code_flutter/src/provider/session_provider.dart';
import 'package:qr_bar_code_flutter/src/utils/DAWidgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:http/http.dart' as https;
// import 'http_routes.dart';

class HttpProvider {
  SessionProvider prefs = new SessionProvider();
  
  String _url = 'b3903.online-server.cloud:8080';
  String _db = 'LOGISTICA';
  bool _cargando = false;

  getHeaders(SessionProvider prefs) {
    return {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json; charset=utf-8',
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
      'Authorization': 'Bearer ' + prefs.authToken!,
    };
  }

  getHeadersMultipart(SharedPreferences prefs) {
    return {
      'Access-Control-Allow-Origin': '*',
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
      'Authorization': 'Bearer ' + prefs.getString("AuthToken")!,
    };
  }

  final _client = HttpClientWithInterceptor.build(interceptors: [
    // Interceptor(),
  ]);

  String getEndPoinds(String sp) {
    switch (sp) {

      case 'upload':
        return 'api/upload';

      case 'download':
        return 'api/download';

      case 'spEmpresaAPP':
        return 'api/' + _db + '/sp/spEmpresaAPP';

      case 'spWebRolApp':
        return 'api/' + _db + '/sp/spWebRolApp';

      case 'spAppAutMovLista':
        return 'api/' + _db + '/sp/spAppAutMovLista';

      case 'spMovDApp':
        return 'api/' + _db + '/sp/spMovDApp';

      case 'spAppAutModuloAutorizacion':
        return 'api/' + _db + '/sp/spAppAutModuloAutorizacion';

      default:
        return '';
    }
  }



  Future test() async {
    var res;
    try {
      var apiUri = prefs.apiUri;
      final Uri loginUri = Uri.parse(apiUri! + "/Help");

      res = await _client.get(loginUri).timeout(Duration(seconds: 5));
    } catch (e) {
      throw "Uri Inválida.";
    }
    return res;
  }

  Future<bool> login(LoginRequestModel requestModel,
      GlobalKey<ScaffoldState> _scaffoldKey) async {
    var res;
    try {
      final Uri loginUri = Uri.parse("http://" + _url + "/api/"+_db+"/Login");
      //final String data = grantPassword(credenciales, licence);
    
      res = await _client
          .post(
            loginUri,
            headers: {
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
              "Access-Control-Allow-Headers": "Content-Type",
              "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            },
            body: jsonEncode(<String, String>{
              'Username': requestModel.username!.toUpperCase(),
              'Password': requestModel.password!.toUpperCase()
            }),
          )
          .timeout(Duration(seconds: 25));

      if (res.statusCode == 200) {
        Response resp = responseFromJsonOne(res.body);

        if (resp.ok == null) {
          prefs.authToken = resp.authToken;
          return true;
        } else {
          _showToast(resp.mensaje, _scaffoldKey);
          return false;
        }
      } else {
        _showToast(res.body, _scaffoldKey);
        return false;
      }
      //return LoginResponseModel.fromJson(jsonDecode(res.body));
    } catch (e) {
      _showToast(res.body, _scaffoldKey);
      print("Servidor no válido. $e");
      return false;
    }
  }

  Future refresh(SessionModel credenciales) async {
    var res;
    try {
      var apiUri = prefs.apiUri;
      final Uri loginUri = Uri.parse(apiUri! + "/Login");
      final String data = grantRefresh(credenciales);

      res = await _client.post(
        loginUri,
        body: data,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
      ).timeout(Duration(seconds: 25));
    } catch (e) {
      throw e;
    }
    return res;
  }



  Future<List<Response>> spAppUpdateToken(
      String? usuarioWeb, String token, int cerrar) async {
    //await prefs.validaRefresh();
    final url = Uri.http(_url, getEndPoinds('spAppUpdateToken'));

    print("URL: " + _url.toString());
    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, dynamic>{
            "UsuarioWeb": usuarioWeb,
            "Token": token,
            //"Cerrar": cerrar
          }),
        )
        .timeout(Duration(seconds: 25));
    print("RESPONSE spAppUpdateToken");
    print(resp.body);
    if (resp.statusCode == 200) {
      return responseFromJson(resp.body);
    } else {
      return [];
    }
  }


  Future<List<Uen>> spWebUENLista(
      String empresa, GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spWebUENLista'));
    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "Email": prefs.usuario,
            "Empresa": empresa,
          }),
        )
        .timeout(Duration(seconds: 25));

    log(resp.body);
    if (resp.statusCode == 200) {
      return uenFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }


  Future<List<Rol>> spWebRolApp(String? webUsuario, GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spWebRolApp'));
    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "WebUsuario": prefs.usuario,
          }),
        )
        .timeout(Duration(seconds: 25));

    log(resp.body);

    if (resp.statusCode == 200) {
      return rolFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }

  
  Future<List<Detalle>> spMovDApp(String? baseDatos,String modulo,String id, GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spMovDApp'));
    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "BaseDatos": baseDatos,
            "Modulo": modulo,
            "ID": id,
          }),
        )
        .timeout(Duration(seconds: 25));

    log(resp.body);

    if (resp.statusCode == 200) {
      return detalleFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }

    Future<List<OkRef>> spAppAutModuloAutorizacion(String? baseDatos,String modulo,String id, GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spAppAutModuloAutorizacion'));
    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "BaseDatosDestino": baseDatos,
            "Modulo": modulo,
            "ID": id,
            "WebUsuario":  prefs.usuario,
          }),
        )
        .timeout(Duration(seconds: 25));

    log(resp.body);

    if (resp.statusCode == 200) {
      return okrefFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }

  



  Future<List<Empresa>> empresas(GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spEmpresaAPP'));


    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "WebUsuario": prefs.usuario,
          }),
        )
        .timeout(Duration(seconds: 25));

    print(resp.body);

    if (resp.statusCode == 200) {
      return empresaFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }

  Future<List<Empresa>> spAppSucursal(
      String empresa, GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spAppSucursal'));

    print(url);

    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "Email": prefs.usuario,
            "Empresa": empresa,
          }),
        )
        .timeout(Duration(seconds: 25));

    log(resp.body);
    log(resp.statusCode.toString());

    if (resp.statusCode == 200) {
      return empresaFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }

    Future<List<Movimiento>> spAppAutMovLista(String empresa,String modulo, GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spAppAutMovLista'));

    log(url.toString());
    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "WebUsuario": prefs.usuario,
            "Modulo": modulo,
            "Sucursal": '0',
            "Empresa": empresa,
            //"BaseDatos": "RTORRESLOG"
          }),
        )
        .timeout(Duration(seconds: 25));

    log(resp.body);
    log(prefs.usuario.toString() );
    log(modulo );
    log(empresa );
    

    if (resp.statusCode == 200) {
      return movimientoFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }


  _showToast(dynamic resp, GlobalKey<ScaffoldState> _scaffoldKey) {
    try {
      if(!resp is String){
      List<Response> res = resp;
      String mensaje = res[0].okRef!.replaceAll('<BR>', "\n");
      DAToast(_scaffoldKey, mensaje);
      }else{
        DAToast(_scaffoldKey, resp);
      }
    } catch (e) {
      DAToast(_scaffoldKey, "Error al hacer la consulta");
    }
  }

  Future<List<OkRef>> spRegisterSw(
      String token, GlobalKey<ScaffoldState> _scaffoldKey) async {
    final url = Uri.http(_url, getEndPoinds('spRegisterSw'));

    print(url);

    final resp = await https
        .post(
          url,
          headers: getHeaders(prefs),
          body: jsonEncode(<String, String?>{
            "Email": prefs.usuario,
            "Token": token,
          }),
        )
        .timeout(Duration(seconds: 25));

    log("TOKEN_FIREBASE: " + resp.body);

    if (resp.statusCode == 200) {
      return okrefFromJson(resp.body);
    } else {
      _showToast(resp.body, _scaffoldKey);
      return [];
    }
  }

}
