import 'dart:async';
import 'dart:developer';

import 'package:qr_bar_code_flutter/src/models/login_model.dart';
import 'package:flutter/material.dart';
import 'package:qr_bar_code_flutter/src/http/http_provider.dart';
import 'package:qr_bar_code_flutter/src/pages/list_jefe_page.dart';
import 'package:qr_bar_code_flutter/src/utils/DAWidgets.dart';
import 'package:qr_bar_code_flutter/src/provider/session_provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // Para agilizar snackbar y loading
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final _prov = new SessionProvider();

  bool _saving = false; // Para estatus loading
  late LoginRequestModel requestModel;

  String invernadero = "assets/invernadero.jpg";

  Future<LoginResponseModel>? responseModel;
  final httpProv = new HttpProvider();

  // Form controllers
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();

  // API
  TextEditingController _apiController = new TextEditingController();
  void onValueChange() {
    setState(() {
      _apiController.text;
    });
  }

  @override
  void initState() {
    super.initState();
    _apiController.text = _prov.apiUri!;
    _apiController.addListener(onValueChange);
    requestModel = new LoginRequestModel();
  }

  @override
  Widget build(BuildContext context) {
    return DaScaffoldLoading(
      isLoading: _saving,
      keyLoading: _scaffoldKey,
      // floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      //floatingActionButton: _configAPI(),
      children: <Widget>[
        _background(),
        _loginForm(),
      ],
    );
  }

  // Fondo
  Widget _background() {
    // Obtiene datos de main.dart, logo y nombre de app
    return DaBackground(
      label: "Finka",
      image: Image.asset(
        "assets/maserplogo.png",
        height: 100,
        width: 250,
        fit: BoxFit.contain,
      ),
    );
  }

  // Login
  Widget _loginForm() {
    return DaFloatingForm(
      title: 'Ingresa con tu cuenta',
      formKey: _formKey,
      children: <Widget>[
        DAInput(
          tipo: 'email',
          label: 'Usuario',
          controller: _emailController,
          onSaved: (value) {
            requestModel.username = value;
          },
        ),
        SizedBox(height: 20.0),
        DAInput(
          tipo: 'password',
          label: 'Contraseña',
          controller: _passController,
          onSaved: (value) {
            requestModel.password = value;
          },
        ),
        SizedBox(height: 40.0),
        // Login Button
        DAButton(
          label: 'Ingresar',
          // onPressed: () async => await _login(),
          onPressed: () => _login(),
        ),
        /*TextButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => RegistrarsePage()));
            },
            child: new Text("Registrar",
                style: new TextStyle(
                  color: Color(0xFFFE9918),
                )))*/
      ],
    );
  }

  /*Future<void> _login() async {
    // setState(() async {
    final form = _formKey.currentState;
    form.save();

    if (!_prov.connected) {
      DAToast(_scaffoldKey, 'Requiere configurar un API válida');
      return;
    }
    

    if (form.validate()) {
      try {
        setState(() => _saving = true); // Loading start

        final httpProv = new HttpProvider();
        var res = await httpProv.login(_modelForm, licence);

        if (res.statusCode == 200) {
          //almacenar refresh token y access token
          var respuesta = json.decode(res.body);
          _prov.session = sessionModelFromLogin(respuesta);
          _formKey.currentState.reset();
          Navigator.pushReplacementNamed(context, 'home');
        } else if (res.statusCode == 400) {
          var respuesta = json.decode(res.body);
          String msg = respuesta["error_description"].toString();
          DAToast(_scaffoldKey, msg);
          //_snakeBar(msg);
        } else {
          //ERROR NO CONTROLADO DESDE EL API
          String msg = res.reasonPhrase.toString();
          DAToast(_scaffoldKey, 'Error de servidor: $msg');
        }

        setState(() => _saving = false); // Loading end
      } catch (e) {
        setState(() => _saving = false); // Loading end
        DAToast(_scaffoldKey, e.toString());
      }
    }
  }*/

  _login() async {
    final form = _formKey.currentState!;
    form.save();
    setState(() {
      _saving = true;
    });
    try {
      final httpProv = new HttpProvider();
      var res = await httpProv.login(requestModel, _scaffoldKey);
      // Loading end
      if (res) {
        _prov.usuario = requestModel.username;
        _spWebRolApp(requestModel.username!);
        setState(() => _saving = false);
      } else {
        setState(() => _saving = false);
        DAToast(
            _scaffoldKey, "Las credenciales proporcionadas no son válidas.");
      }
    } catch (e) {
      //  setState(() => _saving = false); // Loading end
      // DAToast(_scaffoldKey, e.toString());
    }
  }

  Future<void> _spWebRolApp(String usuario) async {
    setState(() {
      _saving = true;
    });
    try {
      final httpProv = new HttpProvider();
      var res = await httpProv.spWebRolApp(usuario, _scaffoldKey);
      setState(() => _saving = false); // Loading end
      if (res.length > 0) {
        log(res[0].appAutCompra.toString());
        log(res[0].appAutCompra.toString());
        log(res[0].appAutCompra.toString());
        _prov.rol = res[0];
        setState(() {
          _saving = false;
        });
        Navigator.of(context).popUntil(ModalRoute.withName('/'));
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ListJefePage()));
      } else {
        setState(() => _saving = false);
        DAToast(_scaffoldKey, "Error al traer el rol");
      }
    } catch (e) {
      setState(() => _saving = false); // Loading end
      DAToast(_scaffoldKey, e.toString());
    }
  }
}
