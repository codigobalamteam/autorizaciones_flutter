import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

// SHA1: 1D:4F:38:94:62:06:9F:C6:75:A7:73:BD:E4:B0:DA:27:80:B1:9D:F0
// P8 - KeyID: VYZH37GGZ9

class PushNotificationService {
  static FirebaseMessaging messaging = FirebaseMessaging.instance;
  static String? token;
  static StreamController<List<String>> _messageStream =
      new StreamController.broadcast();
  static Stream<List<String>> get messagesStream => _messageStream.stream;

  // ON BACKGROUND HANDLER

  static Future _backgroundHandler(RemoteMessage message) async {
    print('onBackground Handler ${message.data}');

    List<String> data = [
      message.data['ID'],
      message.data['Empresa'],
    ];

    _messageStream.add(data);
  }

  static Future _onMessageHandler(RemoteMessage message) async {
    print('onMessage Handler ${message.data}');

    List<String> data = [
      message.data['ID'],
      message.data['Empresa'],
    ];

    _messageStream.add(data);
  }

  static Future _onMessageOpenApp(RemoteMessage message) async {
    print('onMessageOpenApp Handler ${message.data}');

    List<String> data = [
      message.data['ID'],
      message.data['Empresa'],
    ];

    _messageStream.add(data);
  }

  static Future initializeApp() async {
    // Push Notifications
    await Firebase.initializeApp();
    await requestPermission();

    token = await FirebaseMessaging.instance.getToken();
    print('Token: $token');

    // Handlers
    FirebaseMessaging.onBackgroundMessage(_backgroundHandler);
    FirebaseMessaging.onMessage.listen(_onMessageHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenApp);

    // Local Notifications
  }

  // Apple / Web
  static requestPermission() async {
    NotificationSettings settings = await messaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true);

    print('User push notification status ${settings.authorizationStatus}');
  }

  static closeStreams() {
    _messageStream.close();
  }
}
