import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_bar_code_flutter/src/http/http_provider.dart';
import 'package:qr_bar_code_flutter/src/models/notificacion_model.dart';
import 'package:qr_bar_code_flutter/src/models/soporte_cliente_model.dart';
import 'package:qr_bar_code_flutter/src/models/tickets_pendientes_model.dart';
import 'package:qr_bar_code_flutter/src/pages/detail_jefe_page.dart';
import 'package:qr_bar_code_flutter/src/provider/session_provider.dart';
import 'package:qr_bar_code_flutter/src/utils/DAWidgets.dart';
import 'package:qr_bar_code_flutter/src/utils/utils.dart';
import 'package:qr_bar_code_flutter/src/widgets/navbar.dart';
import 'package:qr_bar_code_flutter/src/widgets/text_icon_widet.dart';

class ReceivedNotifications extends StatefulWidget {
  /* var city;
  var country;
  ReceivedNotifications(this.city, this.country);*/

  @override
  _ReceivedNotificationsState createState() => _ReceivedNotificationsState();
}

class _ReceivedNotificationsState extends State<ReceivedNotifications> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _saving = false;
  String ticketID = '';
  String ticketempresa = '';
  List<TicketsPendientes> _movimientoTicket = [];

  //List<ReporteTicket> _movimiento = [];
  final _prov = new SessionProvider();



  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      final NotificacionModel args =
          ModalRoute.of(context)!.settings.arguments as NotificacionModel;
      print(args.empresa);
      print(args.id);
      ticketID = args.id;
      ticketempresa = args.empresa;
      //ticketID = arg;
      this._spWebSoporteID();
    });
  }

  @override
  Widget build(BuildContext context) {
    return DaScaffoldLoading(isLoading: _saving, keyLoading: _scaffoldKey,
        // backgroundColor:  Color.fromRGBO(255, 255, 240, 1),
        children: [
          Scaffold(
            appBar: NavBarCustom("Notificación", false),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    child: Column(
                      children: [
                        _movimientoTicket.length > 0 ? _getCardDos() : Center(),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )

          //Text(arg.toString()),
        ]);
  }

  Future<void> _spWebSoporteID() async {
    setState(() {
      _saving = true;
    });

    try {
    /*  final httpProv = new HttpProvider();
     var res = await httpProv.spWebSoporteID(
          ticketempresa.toString(), ticketID.toString(), _scaffoldKey);
      setState(() => _saving = false); // Loading end
      if (res.length > 0) {
        setState(() {
          _saving = false;
          _movimientoTicket = res;
        });
      } else {
        setState(() {
          _saving = false;
        });
        DAToast(_scaffoldKey, "Sin información");
        //setFilterSucursal(_movimientos);
      }*/
    } catch (e) {
      setState(() => _saving = false); // Loading end
      DAToast(_scaffoldKey, e.toString());
    }
  }

  Widget _getCardDos() {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      child: ClipPath(
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(254, 254, 254, .9)),
          child: Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                      color: getColorTicket(_movimientoTicket[0])),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: new EdgeInsets.all(8),
                          child: Text(
                            "Folio:" + _movimientoTicket[0].movId.toString(),
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                      IconButton(
                        icon: const Icon(
                          Icons.info_outline,
                          color: Colors.white,
                        ),
                        tooltip: 'Detalle',
                        onPressed: () {
                          _nextPage(_prov.usuario);
                        },
                      ),
                    ],
                  )),
              ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                /* leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                border: new Border(right: new BorderSide(width: 1.0, color: CupertinoTheme.of(context).primaryColor))),
                child: Icon(Icons.autorenew, color:  CupertinoTheme.of(context).primaryColor),
            ),*/
                title: Column(
                  children: [
                    Row(
                      children: [
                        textWithIcon("Situación", _movimientoTicket[0].situacion.toString(),
                            Icons.check_outlined, 2,false),
                        textWithIcon(
                            "Fecha Emisión",
                            formatDate2(_movimientoTicket[0].fechaEmision),
                            Icons.calendar_today,
                            2,false),
                      ],
                    ),
                    Divider(height: 20, color: Color(0xFF006c3f)),
                    Container(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Row(
                        children: [
                          textWithIcon(
                              "Titulo",
                              _movimientoTicket[0].titulo.toString(),
                              Icons.text_fields,
                              2,false),
                          textWithIcon(
                              "Fecha de Vencimiento",
                              formatDate2(_movimientoTicket[0].vencimiento),
                              Icons.calendar_today,
                              2,false),
                        ],
                      ),
                    ),
                  _prov.rol != "Usuario"? Divider(
                        height: 20,
                        color:
                            CupertinoTheme.of(context).primaryContrastingColor) : Center(),
                  _prov.rol != "Usuario" ? textWithIcon(
                        "Solicitado por",
                        _movimientoTicket[0].usuarioNombre.toString(),
                        Icons.update,
                        1,false) : Center(),
                    /*Divider(
                      height: 20,
                      color:
                          CupertinoTheme.of(context).primaryContrastingColor),
                  textWithIcon("UEN", movimiento.nombreUEN!,
                      Icons.update, 1),*/
                  ],
                ),
                // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
              ),
            ],
          ),
        ),
        clipper: ShapeBorderClipper(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10))),
      ),
    );
  }

  _nextPage(String? rol) {
    switch (rol) {
      
     


      case "Jefe Mantenimiento":
        
        Navigator.pop(context);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailJefePage(_movimientoTicket[0])));

        return;

      default:
    }
  }

  Color getColorTicket(TicketsPendientes movimiento) {
    Color color = Colors.green;
    try {
      DateTime fechaEmision = movimiento.fechaEmision;
      DateTime fechaVencimiento = movimiento.vencimiento;
      DateTime now = DateTime.now();
      final differenceTotal = fechaVencimiento.difference(fechaEmision).inDays;
      final differenceToday = now.difference(fechaEmision).inDays;

      final periodo = differenceTotal / 3;

      if ((0 <= differenceToday) && (differenceToday < periodo)) {
        color = Colors.green;
      } else if ((periodo <= differenceToday) &&
          (differenceToday <= (periodo * 2))) {
        color = Colors.yellow[700]!;
      } else if (((periodo * 2) <= differenceToday) &&
          (differenceToday <= (periodo * 3))) {
        color = Colors.red;
      } else if (((periodo * 3) <= differenceToday)) {
        color = Colors.red;
      }
      return color;
    } catch (e) {
      return color;
    }
  }
}
