import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:group_button/group_button.dart';
import 'package:qr_bar_code_flutter/src/http/http_provider.dart';
import 'package:qr_bar_code_flutter/src/models/detalle_model.dart';
import 'package:qr_bar_code_flutter/src/models/empresa_model.dart';
import 'package:qr_bar_code_flutter/src/models/movimiento_model.dart';
import 'package:qr_bar_code_flutter/src/provider/session_provider.dart';
import 'package:qr_bar_code_flutter/src/utils/DAWidgets.dart';
import 'package:qr_bar_code_flutter/src/utils/utils.dart';
import 'package:qr_bar_code_flutter/src/widgets/AppBarImage.dart';
import 'package:qr_bar_code_flutter/src/widgets/search_widget.dart';
import 'package:qr_bar_code_flutter/src/widgets/text_icon_widet.dart';
import 'package:unique_list/unique_list.dart';

import 'close_session_page.dart';

class ListJefePage extends StatefulWidget {
  ListJefePage({Key? key}) : super(key: key);

  @override
  _ListJefePageState createState() => _ListJefePageState();
}

class _ListJefePageState extends State<ListJefePage> {
  late Widget cusSearchBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _saving = false;
  final httpProv = new HttpProvider();
  List<Movimiento> _movimientos = [];
  List<Movimiento> _movimientosFiltered = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  late Empresa empresa;
  List<Empresa> _empresas = [];
  List<String> _sucursales = [];
  List<String> _grupoTrabajo = [];
  List<String> _movimientosTipo = [];

  List<IconData> _urgencias = [Icons.ballot, Icons.ballot, Icons.ballot];
  IconData _urgencia = Icons.ballot;

  late String sucursal = "Todas";
  late String grupoTrabajo = "Todos";
  late String _tipomov = "Todos";
  late String _movimientoTipo = "";
  int _estatusSelected = 1;
  String filteText = "";
  String imagenAppBar = "assets/invernaderobar.jpg";
  double _sizeBoton = 0;
  FaIcon cusIcon = FaIcon(FontAwesomeIcons.search);
  List<Movimiento> _tipoMovimientos = [];
  int _condicional = 0;
  String query = '';
  List<String> listMov = [];
  final _prov = new SessionProvider();
  String? _opcionSeleccionadaMovimiento;

  @override
  Widget build(BuildContext context) {
    return DaScaffoldLoading(isLoading: _saving, keyLoading: _scaffoldKey,
        // backgroundColor:  Color.fromRGBO(255, 255, 240, 1),
        children: [_getBody()]);
  }

  Widget _getBody() {
    return Stack(
      children: [
        /* Image.asset(
              "assets/backgroundRtorres.jpg",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
              ),*/
        Scaffold(
          //floatingActionButton: _botonFloating(),
          backgroundColor: Color.fromRGBO(255, 255, 255, 1),
          //backgroundColor: Colors.transparent,

         /* appBar: PreferredSize(
            preferredSize: const Size.fromHeight(10),
            child: AppBarImage(),
          ),*/
          body: Column(
            children: [
              AppBarImage(),
              Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      elevation: 10,
                      //color: Colors.transparent,
                      margin: EdgeInsets.all(8),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(children: [
                          Row(
                            children: [
                              Expanded(child: buildSearch()),
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: ClipOval(
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: 40,
                                    height: 40,
                                    color: Colors.blue,
                                    padding: EdgeInsets.all(8),
                                    child: Text(
                                      _movimientosFiltered.length.toString(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 13),
                                    ),
                                  ),
                                ),
                              ),
                              _getButtonLogOut()
                            ],
                          ),
                          query == ""
                              ? Column(children: [
                                  Row(
                                    children: [
                                      _empresas.length > 0
                                          ? Expanded(
                                              child: Center(
                                                child: DropdownButton<Empresa>(
                                                  value: empresa,
                                                  //icon:const Icon(Icons.house),
                                                  //iconSize: 24,
                                                  elevation: 16,
                                                  style: const TextStyle(
                                                      color: Colors.blue),
                                                  underline: Container(
                                                    height: 2,
                                                    color: Colors.white,
                                                  ),
                                                  onChanged:
                                                      (Empresa? newValue) {
                                                    setState(() {
                                                      empresa = newValue!;
                                                    });

                                                    _spAppAutMovLista();
                                                  },
                                                  items: _empresas.map<
                                                          DropdownMenuItem<
                                                              Empresa>>(
                                                      (Empresa value) {
                                                    return DropdownMenuItem<
                                                        Empresa>(
                                                      value: value,
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(0.0),
                                                          child: Text(value
                                                              .empresa
                                                              .toString()),
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.20,
                                                        ),
                                                      ),
                                                    );
                                                  }).toList(),
                                                ),
                                              ),
                                            )
                                          : Center(),
                                      GroupButton(
                                          isRadio: true,
                                          spacing: 10,
                                          onSelected: (index, isSelected) => {
                                                print(
                                                    '$index button is ${isSelected ? 'selected' : 'unselected'}'),
                                                setState(() {
                                                  _movimientoTipo =
                                                      listMov[index];
                                                }),
                                                _spAppAutMovLista()
                                              },
                                          buttons: listMov,
                                          borderRadius:
                                              BorderRadius.circular(60.0),
                                          selectedColor: Colors.blueGrey,
                                          selectedShadow: <BoxShadow>[
                                            BoxShadow(color: Colors.transparent)
                                          ],
                                          unselectedShadow: <BoxShadow>[
                                            BoxShadow(color: Colors.transparent)
                                          ]),
                                    ],
                                  ),

                                  /* Row(
                                              children: [
  
      
                                                _sucursales.length > 0
                                                    ? Expanded(
                                                        child: Center(
                                                          child: DropdownButton<String>(
                                                            value: sucursal,
                                                            icon: FaIcon(FontAwesomeIcons.building,color: Colors.grey,),
                                                            iconSize: 13,
                                                            elevation: 16,
                                                            style: const TextStyle(color: Colors.blue,),
                                                            underline: Container(height: 2,color: Colors.white,),

                                                            onChanged:
                                                                (String? newValue) {
                                                              setState(() {
                                                                sucursal = newValue!;
                                                              });
                                                              _filtroSucursales();
                                                            },
                                                            items: _sucursales.map<DropdownMenuItem<String>>(
                                                              
                                                                (String value) {
                                                              return DropdownMenuItem<String>(
                                                                value: value,
                                                                child: Container(
                                                                  padding: const EdgeInsets.all(4.0),
                                                                  child: Text(value,style: const TextStyle(fontSize:14), overflow: TextOverflow.ellipsis,),
                                                                  width: MediaQuery.of(context).size.width * 0.24,),
                                                                  
                                                              );
                                                            }).toList(),
                                                          ),
                                                        ),
                                                      )
                                                    : Center(),
                                                     _grupoTrabajo.length > 0
                                                    ? Expanded(
                                                        child: Center(
                                                          child: DropdownButton<String>(
                                                            value: grupoTrabajo,
                                                            icon: FaIcon(FontAwesomeIcons.userFriends,color: Colors.grey,),
                                                              iconSize: 13,
                                                            elevation: 16,
                                                            style: const TextStyle(color: Colors.blue),
                                                            underline: Container(height: 2,color: Colors.white,
                                                            ),
                                                            onChanged:
                                                                (String? newValue) {
                                                              setState(() {
                                                                grupoTrabajo = newValue!;
                                                              });
                                                              _filtroSucursales();
                                                            },
                                                            items: _grupoTrabajo.map<DropdownMenuItem<String>>((String value) {
                                                              return DropdownMenuItem<String>(
                                                                value: value,
                                                                child: Container(
                                                                  padding:const EdgeInsets.all(4.0),
                                                                  child: Text(value,style: const TextStyle(fontSize:14,),  overflow: TextOverflow.ellipsis,),
                                                                   width: MediaQuery.of(context).size.width * 0.24,
                                                                ),
                                                              );
                                                            }).toList(),
                                                          ),
                                                        ),
                                                      )
                                                    : Center(),
                                                     _movimientosTipo.length > 0
                                                    ? Expanded(
                                                        child: Center(
                                                          child: DropdownButton<String>(
                                                            value: _tipomov,
                                                            icon: FaIcon(FontAwesomeIcons.list,color: Colors.grey,),
                                                            iconSize: 13,
                                                            elevation: 16,
                                                            style: const TextStyle(color: Colors.blue),
                                                            underline: Container(height: 2,color: Colors.white,),
                                                            onChanged:
                                                                (String? newValue) {
                                                              setState(() {
                                                                _tipomov = newValue!;
                                                              });
                                                              _filtroSucursales();
                                                            },
                                                            items: _movimientosTipo.map<DropdownMenuItem<String>>((String value) {
                                                              return DropdownMenuItem<String>(
                                                                value: value,
                                                                child: Container(
                                                                  padding:const EdgeInsets.all(4.0),
                                                                  child: Text(value,style: const TextStyle(fontSize:14),  overflow: TextOverflow.ellipsis, ),
                                                                  width: MediaQuery.of(context).size.width * 0.24,
                                                                ),
                                                              );
                                                            }).toList(),
                                                          ),
                                                        ),
                                                      )
                                                    : Center(),
                                              ],
                                            ),*/
                                ])
                              : Divider()
                        ]),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(child: _getList())
            ],
          ),
        )
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _getEmpresa();
    selectMov();
  }

  selectMov() {
    listMov = [];

    print(_prov.rol.appAutGasto);
    print(_prov.rol.appAutCompra);
    print(_prov.rol.appAutGasto);

    //if(_prov.rol.appAutGasto == true)
    if (true) {
      listMov.add("GASTOS");
      if (_movimientoTipo == "") _movimientoTipo = "GAS";
    }
    //if(_prov.rol.appAutCompra == true){
    if (true) {
      listMov.add("COMPRAS");
      if (_movimientoTipo == "") _movimientoTipo = "COM";
    }
    //if(_prov.rol.appAutCxp == true){
    if (false) {
      listMov.add("CXP");
      if (_movimientoTipo == "") _movimientoTipo = "CXP";
    }

    _spAppAutMovLista();
  }

  Future<Null> refreshList() async {
    _spAppAutMovLista();

    refreshKey.currentState!.show(atTop: false);
    return null;
  }

  _botonFloating() {
    return FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
        onPressed: () {
          /* Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => LevantarTicket("Jefe"),
            ),
          );*/
        });
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Buscar ...',
        onChanged: searchMov,
      );

  void searchMov(String query) {
    final _movimientosFiltered = _movimientos.where((movs) {
      final id = movs.movId.toString().toLowerCase();
      final nombre = movs.nombre.toString().toLowerCase();
      final subtotal = movs.subTotal.toString().toLowerCase();
      final searchLower = query.toLowerCase();
      final referencia = movs.referencia.toLowerCase();
      final observaciones = movs.observaciones.toString().toLowerCase();
      final proveedor = movs.provNombre.toString().toLowerCase();

      return id.contains(searchLower.toLowerCase()) ||
          nombre.contains(searchLower.toLowerCase()) ||
          sucursal.contains(subtotal) ||
          referencia.contains(searchLower.toLowerCase()) ||
          observaciones.contains(searchLower.toLowerCase()) ||
          proveedor.contains(searchLower.toLowerCase());
      ;
    }).toList();

    setState(() {
      this.query = query;
      this._movimientosFiltered = _movimientosFiltered;
    });
  }

  Widget _getList() {
    return Container(
      height: MediaQuery.of(context).size.height * 1,
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
      width: MediaQuery.of(context).size.width,
      child: _movimientosFiltered.length < 1
          ? EmptyList()
          : RefreshIndicator(
              child: ListView.builder(
                itemCount: _movimientosFiltered.length,
                itemBuilder: (context, i) {
                  return _getCard(_movimientosFiltered[i]);
                },
              ),
              onRefresh: refreshList,
            ),
    );
  }

  Widget _getListDetail(List<Detalle> detalleList) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: detalleList.length,
        itemBuilder: (context, i) {
          return _getCardDetail(detalleList[i]);
        },
      ),
    );
  }

  Widget _getCard(Movimiento movimiento) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      child: ClipPath(
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(254, 254, 254, .9)),
          child: Column(
            children: [
              Container(
                  decoration: BoxDecoration(gradient: LinearGradient(
                   colors: [ Color.fromRGBO(9, 31, 146, 1),Colors.blue],
                    begin: Alignment.bottomRight,
                    end: Alignment.topLeft,
                  ),
                ),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: new EdgeInsets.all(8),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 8.0, right: 8.0, bottom: 8.0, top: 8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        child: SelectableText(
                                      movimiento.mov! + " " + movimiento.movId!,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    )),
                                    Text(
                                      formatDate2(movimiento.fechaEmision),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 6.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SelectableText(
                                            formatCurrencyString(movimiento
                                                    .subTotal
                                                    .toString()) +
                                                " " +
                                                movimiento.moneda.toString(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16),
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 6.0),
                                            child: SelectableText(
                                              movimiento.provNombre.toString(),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15),
                                            ),
                                          ),
                                        ],
                                      )),
                                      _getButtonInfo(movimiento),
                                      _getButtonCheck(movimiento)
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
              ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Column(
                  children: [
                    Row(
                      children: [
                        textWithIcon("Estatus", movimiento.estatus!,
                            FontAwesomeIcons.check, 2, false),
                        textWithIcon("Situación", movimiento.situacion!,
                            FontAwesomeIcons.list, 2, false),
                      ],
                    ),
                    Divider(height: 12, color: Colors.blueAccent),
                    Container(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Row(
                        children: [
                          textWithIcon(
                              "Usuario",
                              movimiento.nombre!
                                  .toString()
                                  .replaceAll("  ", ""),
                              FontAwesomeIcons.userAlt,
                              2,
                              false),
                          textWithIcon(
                              "Sucursal",
                              movimiento.sucursalNombre.toString(),
                              FontAwesomeIcons.solidBuilding,
                              2,
                              false),
                        ],
                      ),
                    ),
                    Divider(height: 12, color: Colors.blueAccent),
                    movimiento.referencia!.length != "" ||
                            movimiento.observaciones!.length != ""
                        ? Container(
                            padding: EdgeInsets.only(top: 8.0),
                            child: Row(
                              children: [
                                textWithIcon(
                                    "Referencia",
                                    movimiento.referencia! != ""
                                        ? movimiento.referencia!
                                        : movimiento.observaciones!,
                                    FontAwesomeIcons.envelopeOpenText,
                                    2,
                                    false),
                              ],
                            ),
                          )
                        : Center(),
                  ],
                ),
              ),
            ],
          ),
        ),
        clipper: ShapeBorderClipper(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20))),
      ),
    );
  }

  Widget _getCardDetail(Detalle detalle) {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(color: Colors.white),
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    margin: new EdgeInsets.all(8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 2.0, bottom: 2.0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: SelectableText(
                                detalle.concepto!,
                                style:
                                    TextStyle(color: Colors.blue, fontSize: 16),
                              )),
                              SelectableText(
                                detalle.cantidad.toString(),
                                style: TextStyle(
                                    color: Colors.black, fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 2.0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Container(
                                      child: SelectableText(
                                detalle.descripcion!,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12),
                              ))),
                              Container(
                                  alignment: Alignment.centerRight,
                                  width: 110,
                                  child: SelectableText(
                                    formatCurrencyString(
                                        detalle.precio.toString()),
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  )),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 2.0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(
                                detalle.descripcion!,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12),
                              )),
                              Container(
                                  alignment: Alignment.centerRight,
                                  width: 110,
                                  child: SelectableText(
                                    formatCurrencyString(
                                        detalle.subTotal.toString()),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )),
        Divider(
          height: 1,
          color: Colors.blue,
        )
      ],
    );
  }

  Widget _getButtonLogOut() {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.only(right: 18.0, left: 10),
        child: FaIcon(
          FontAwesomeIcons.user,
          color: Colors.grey,
          size: 20,
        ),
      ),
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => CloseSession()));
      },
    );
  }

  Widget _getButtonInfo(Movimiento movimiento) {
    return GestureDetector(
      child: FaIcon(
        FontAwesomeIcons.infoCircle,
        color: Colors.white,
        size: 22,
      ),
      onTap: () {
        _spMovDApp(movimiento);
      },
    );
  }

  Widget _getButtonCheck(Movimiento movimiento) {
    return GestureDetector(
        child: Padding(
          padding: const EdgeInsets.only(left: 18.0),
          child: FaIcon(
            FontAwesomeIcons.checkCircle,
            color: Colors.white,
            size: 22,
          ),
        ),
        onTap: () {
          AwesomeDialog(
              context: context,
              animType: AnimType.LEFTSLIDE,
              headerAnimationLoop: false,
              dialogType: DialogType.QUESTION,
              showCloseIcon: true,
              isDense: true,
              title: "Autorizar Movimiento",
              desc: "¿Desea autorizar " +
                  movimiento.mov! +
                  " " +
                  movimiento.movId! +
                  " de " +
                  movimiento.usuario! +
                  " por " +
                  formatCurrencyString(movimiento.subTotal.toString()) +
                  " " +
                  movimiento.moneda.toString() +
                  "?",
              btnOkOnPress: () {
                _spAutorizacionModuloAPP(movimiento);
              },
              btnOkText: "Autorizar",
              btnCancelOnPress: () {},
              btnOkIcon: Icons.check_circle,
              btnCancelText: "Cancelar",
              onDissmissCallback: (type) {
                debugPrint('Dialog Dissmiss from callback $type');
              })
            ..show();
        });
  }

  Future<void> _getEmpresa() async {
    setState(() {
      _saving = true;
    });
    try {
      final httpProv = new HttpProvider();
      var res = await httpProv.empresas(_scaffoldKey);
      setState(() => _saving = false); // Loading end
      if (res.length > 0) {
        Empresa currentEmpresa = res[0];

        for (var i = 0; i < res.length; i++) {
          print(res[i].empresa.toString());
          if (res[i].empresa.toString().contains(_prov.empresa!)) {
            currentEmpresa = res[i];
          }
        }

        setState(() {
          _saving = false;
          empresa = currentEmpresa;
          _empresas = res;
        });
        _spAppAutMovLista();
      } else {
        setState(() => _saving = false);
        DAToast(_scaffoldKey, "Error al traer las Empresas");
      }
    } catch (e) {
      setState(() => _saving = false); // Loading end
      DAToast(_scaffoldKey, e.toString());
    }
  }

  Future<void> _spAppAutMovLista() async {
    setState(() {
      _movimientos = [];
      _movimientosFiltered = [];
      filteText = "";
      query = "";
      _sucursales = [];
      _grupoTrabajo = [];
      grupoTrabajo = "Todos";
      sucursal = "Todas";
      _tipoMovimientos = [];
      _movimientosTipo = [];
      _tipomov = "Todos";
      _saving = true;
    });
    try {
      var movimientoTipo = "GAS";
      if(_movimientoTipo == "COMPRAS"){
          movimientoTipo = "COMS";
      }
      final httpProv = new HttpProvider();
      var res = await httpProv.spAppAutMovLista(
          empresa.empresa.toString(), movimientoTipo, _scaffoldKey);
      setState(() => _saving = false); // Loading end
      if (res.length > 0) {
        setState(() {
          _saving = false;
          _movimientos = res;
          _movimientosFiltered = _movimientos;
          query = "";
          filteText = "";
          _estatusSelected = 0;
        });
        setFilterSucursal(_movimientos);
        setFilterGrupoTrabajo(_movimientos);
        setFilterMovimientos(_movimientos);
      } else {
        setState(() {
          _saving = false;
          _sucursales = [];
          _grupoTrabajo = [];
          _movimientosTipo = [];
        });
        DAToast(_scaffoldKey, "Sin información");
        //setFilterSucursal(_movimientos);
      }
    } catch (e) {
      setState(() => _saving = false); // Loading end
      DAToast(_scaffoldKey, e.toString());
    }
  }

  Future<void> _spMovDApp(Movimiento movimiento) async {
    setState(() {
      _saving = true;
    });
    try {
      final httpProv = new HttpProvider();
      var res = await httpProv.spMovDApp(movimiento.dateBase.toString(),
          movimiento.modulo.toString(), movimiento.id.toString(), _scaffoldKey);
      setState(() => _saving = false); // Loading end
      if (res.length > 0) {
        getDialog(movimiento, res);
        setState(() {
          _saving = false;
        });
      } else {
        setState(() => _saving = false);
        DAToast(_scaffoldKey, "Sin detalle");
      }
    } catch (e) {
      setState(() => _saving = false); // Loading end
      DAToast(_scaffoldKey, e.toString());
    }
  }

  Future<void> _spAutorizacionModuloAPP(Movimiento movimiento) async {
    setState(() {
      _saving = true;
    });
    try {
      final httpProv = new HttpProvider();
      var res = await httpProv.spAppAutModuloAutorizacion(
          movimiento.dateBase.toString(),
          movimiento.modulo.toString(),
          movimiento.id.toString(),
          _scaffoldKey);
      setState(() => _saving = false); // Loading end
      if (res[0].ok == "") {
        _spAppAutMovLista();
        getDialogSucces(movimiento);
        setState(() {
          _saving = false;
        });
      } else {
        setState(() => _saving = false);
        DAToast(_scaffoldKey, res[0].okRef.toString());
      }
    } catch (e) {
      setState(() => _saving = false); // Loading end
      DAToast(_scaffoldKey, e.toString());
    }
  }

  void getDialog(Movimiento movimiento, List<Detalle> detalle) {
    AwesomeDialog(
        context: context,
        animType: AnimType.LEFTSLIDE,
        headerAnimationLoop: false,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(0),
        isDense: true,
        dialogType: DialogType.INFO_REVERSED,
        showCloseIcon: true,
        title: movimiento.mov! + " " + movimiento.movId! + " \n\n ",
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0, bottom: 2),
              child: Text(
                movimiento.mov.toString() + " " + movimiento.movId.toString(),
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 22,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 5.0, bottom: 5, left: 30, right: 30),
                  child: Text(
                    formatCurrencyString(movimiento.subTotal.toString()) +
                        " " +
                        movimiento.moneda.toString(),
                    style: TextStyle(color: Colors.blue, fontSize: 18),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 0, bottom: 5, left: 30, right: 30),
                  child: Text(
                    movimiento.uenNombre.toString(),
                    style: TextStyle(color: Colors.blue, fontSize: 18),
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  height: 10,
                )
              ],
            ),
            _getListDetail(detalle),
          ],
        ),
        btnOkOnPress: () {
          _spAutorizacionModuloAPP(movimiento);
        },
        btnOkText: "Autorizar",
        btnCancelOnPress: () {},
        btnOkIcon: Icons.check_circle,
        btnCancelText: "Cancelar",
        onDissmissCallback: (type) {
          debugPrint('Dialog Dissmiss from callback $type');
        })
      ..show();
  }

  void getDialogSucces(Movimiento movimiento) {
    AwesomeDialog(
        context: context,
        animType: AnimType.LEFTSLIDE,
        headerAnimationLoop: false,
        dialogType: DialogType.SUCCES,
        showCloseIcon: true,
        isDense: true,
        title: "Movimiento Autorizado",
        desc: movimiento.mov! +
            " " +
            movimiento.movId! +
            " de " +
            movimiento.usuario! +
            " por " +
            formatCurrencyString(movimiento.subTotal.toString()) +
            " " +
            movimiento.moneda.toString(),
        btnOkOnPress: () {},
        btnOkText: "Ok",
        btnOkIcon: Icons.check_circle,
        onDissmissCallback: (type) {
          debugPrint('Dialog Dissmiss from callback $type');
        })
      ..show();
  }

  void setFilterSucursal(List<Movimiento> movimientos) {
    _sucursales = [];
    sucursal = "Todas";

    final filterList = UniqueList<String>();
    filterList.clear();
    //filterList.add("Todas");
    movimientos.map((element) {
      return filterList.add(element.sucursalNombre.toString());
    }).toList();

    filterList.sort();
    filterList.insert(0, "Todas");

    setState(() {
      sucursal = "Todas";
      _sucursales = filterList;
    });
  }

  void setFilterGrupoTrabajo(List<Movimiento> movimientos) {
    _grupoTrabajo = [];
    grupoTrabajo = "Todos";

    final filterList = UniqueList<String>();
    filterList.clear();
    //filterList.add("Todas");
    movimientos.map((element) {
      return filterList.add(element.grupoTrabajo.toString());
    }).toList();

    filterList.sort();
    filterList.insert(0, "Todos");

    setState(() {
      grupoTrabajo = "Todos";
      _grupoTrabajo = filterList;
    });
  }

  void setFilterMovimientos(List<Movimiento> movimientos) {
    _movimientosTipo = [];
    _tipomov = "Todos";

    final filterList = UniqueList<String>();
    filterList.clear();
    //filterList.add("Todas");
    movimientos.map((element) {
      return filterList.add(element.mov.toString());
    }).toList();

    filterList.sort();
    filterList.insert(0, "Todos");

    setState(() {
      _tipomov = "Todos";
      _movimientosTipo = filterList;
    });
  }

  void _filtroSucursales() {
    if (sucursal != 'Todas') {
      if (grupoTrabajo != "Todos") {
        if (_tipomov != "Todos") {
          this._movimientosFiltered = _movimientos
              .where((servicio) =>
                  servicio.sucursalNombre == sucursal &&
                  servicio.grupoTrabajo!.compareTo(grupoTrabajo) == 0 &&
                  servicio.mov!.compareTo(_tipomov) == 0)
              .toList();
        } else {
          this._movimientosFiltered = _movimientos
              .where((servicio) =>
                  servicio.sucursalNombre == sucursal &&
                  servicio.grupoTrabajo!.compareTo(grupoTrabajo) == 0)
              .toList();
        }
      } else {
        if (_tipomov != "Todos") {
          this._movimientosFiltered = _movimientos
              .where((servicio) =>
                  servicio.sucursalNombre == sucursal &&
                  servicio.mov!.compareTo(_tipomov) == 0)
              .toList();
        } else {
          this._movimientosFiltered = _movimientos
              .where((servicio) => servicio.sucursalNombre == sucursal)
              .toList();
        }
      }
    } else {
      if (grupoTrabajo != "Todos") {
        if (_tipomov != "Todos") {
          this._movimientosFiltered = _movimientos
              .where((servicio) =>
                  servicio.grupoTrabajo!.compareTo(grupoTrabajo) == 0 &&
                  servicio.mov!.compareTo(_tipomov) == 0)
              .toList();
        } else {
          this._movimientosFiltered = _movimientos
              .where((servicio) =>
                  servicio.grupoTrabajo!.compareTo(grupoTrabajo) == 0)
              .toList();
        }
      } else {
        if (_tipomov != "Todos") {
          _movimientosFiltered = _movimientos
              .where((servicio) => servicio.mov!.compareTo(_tipomov) == 0)
              .toList();
          ;
        } else {
          _movimientosFiltered = _movimientos;
        }
      }
    }
  }

  Widget EmptyList() {
    return Center(
        child: Container(
      height: 130,
      child: Column(
        children: [
          Text("Sin resultados"),
          IconButton(
            icon: FaIcon(
              FontAwesomeIcons.recycle,
              color: Colors.blue,
            ),
            tooltip: 'Favorite',
            onPressed: () {
              _spAppAutMovLista();
            },
          ),
          Text("Click para Recargar")
        ],
      ),
    ));
  }
}
