import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';

import 'package:qr_bar_code_flutter/src/http/http_provider.dart';
import 'package:qr_bar_code_flutter/src/models/okRef_model.dart';
import 'package:qr_bar_code_flutter/src/models/tickets_pendientes_model.dart';
import 'package:qr_bar_code_flutter/src/provider/session_provider.dart';

import 'package:qr_bar_code_flutter/src/utils/DAWidgets.dart';
import 'package:qr_bar_code_flutter/src/utils/utils.dart';
import 'package:qr_bar_code_flutter/src/widgets/navbar.dart';
import 'package:qr_bar_code_flutter/src/widgets/text_icon_widet.dart';

import 'list_jefe_page.dart';

class DetailJefePage extends StatefulWidget {
  final TicketsPendientes movimiento;
  DetailJefePage(this.movimiento);

  @override
  _DetailJefePageState createState() => _DetailJefePageState(movimiento);
}

class _DetailJefePageState extends State<DetailJefePage> {
  TicketsPendientes movimiento;

  _DetailJefePageState(this.movimiento);
  String? qrCodeResult;
  int camera = -1;
  int _initialCard = 0;
  String _currentArt = "";
  PickedFile? _imageFile = null;
  dynamic _pickImageError;
  bool _isVideo = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<ExpansionTileCardState> cardA = new GlobalKey();
  bool _saving = false;
  String? _nameImage = null;
  File? _videoFile;
  bool _activeArticulos = false;
  String? _ruta = null;

  String? _causa = "";

  String dropdownValue = 'Asigna Vencimiento';
  String? _opcionHoraSeleccionadaTecnico;
  String _responseAsignaTecnico = '';
  int _aprobado = 0;

  String? _opcionHoraSeleccionadaVencimiento = "10 Dias";
  final _solucionController = TextEditingController();
  final comentariosController = TextEditingController();
  final _prov = new SessionProvider();

  bool backCamera = false;
  final httpProv = new HttpProvider();

  String imagenAppBar = "assets/invernaderobar.jpg";

  final _descripcionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return DaScaffoldLoading(isLoading: _saving, keyLoading: _scaffoldKey,
        // backgroundColor:  Color.fromRGBO(255, 255, 240, 1),
        children: [_getBody()]);
  }

  @override
  void initState() {
    super.initState();
    //_spWebDesembarqueCausa();
    cardA.currentState?.expand();
    _solucionController.text = movimiento.solucion!;
  }

  Widget _getBody() {
    return Stack(children: [
      /* Image.asset(
              "assets/backgroundRtorres.jpg",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
              ),*/
      Scaffold(
        floatingActionButton: _getFAB(),
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        //backgroundColor: Colors.transparent,
        appBar: NavBarCustom(movimiento.mov! + " " + movimiento.movId!, false),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(8.0),
                child: ExpansionTileCard(
                  key: cardA,
                  initiallyExpanded: true,
                  title: Row(
                    children: [
                      textWithIcon(
                          "Folio", movimiento.movId!, Icons.check_outlined, 2,false),
                      textWithIcon("Técnico", movimiento.nombre!,
                          Icons.supervised_user_circle, 2,false),
                    ],
                  ),
                  subtitle: Divider(
                      height: 20,
                      color:
                          CupertinoTheme.of(context).primaryContrastingColor),
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              textWithIcon("Empresa", movimiento.empresa!,
                                  Icons.check_outlined, 2,false),
                              textWithIcon("Sucursal", movimiento.sucNombre!,
                                  Icons.calendar_today, 2,false),
                            ],
                          ),
                          Divider(
                              height: 20,
                              color: CupertinoTheme.of(context)
                                  .primaryContrastingColor),
                          Row(
                            children: [
                              textWithIcon("Área", movimiento.area!,
                                  Icons.check_outlined, 2,false),
                              textWithIcon(
                                  "Vencimiento",
                                  formatDate2(movimiento.vencimiento!),
                                  Icons.calendar_today,
                                  2,false),
                            ],
                          ),
                          Divider(
                              height: 20,
                              color: CupertinoTheme.of(context)
                                  .primaryContrastingColor),
                          Row(
                            children: [
                              textWithIcon(
                                  "Solicitado por",
                                  movimiento.usuarioNombre!,
                                  Icons.check_outlined,
                                  2,false),
                              textWithIcon(
                                  "Importe",
                                  formatCurrencyString(movimiento.importe!),
                                  Icons.attach_money,
                                  2,false),
                            ],
                          ),
                          Divider(
                              height: 20,
                              color: CupertinoTheme.of(context)
                                  .primaryContrastingColor),
                          textWithIcon("Titulo", movimiento.titulo!,
                              Icons.check_outlined, 1,false),
                          Divider(
                              height: 20,
                              color: CupertinoTheme.of(context)
                                  .primaryContrastingColor),
                          textWithIcon("Descripción", movimiento.problema!,
                              Icons.check_outlined, 1,false),
                          Divider(
                              height: 20,
                              color: CupertinoTheme.of(context)
                                  .primaryContrastingColor),
                          movimiento.motivoRechazo != ""
                              ? textWithIcon(
                                  "Motivo del Rechazo",
                                  movimiento.motivoRechazo!,
                                  Icons.check_outlined,
                                  1,false)
                              : Center(),
                          movimiento.motivoRechazo != ""
                              ? Divider(
                                  height: 20,
                                  color: CupertinoTheme.of(context)
                                      .primaryContrastingColor)
                              : Center(),
                          textWithIcon(
                              "Solución",
                              movimiento.agente!
                                          .toUpperCase()
                                          .replaceAll(" ", "") !=
                                      _prov.usuario!
                                          .toUpperCase()
                                          .replaceAll(" ", "")
                                  ? movimiento.solucion!
                                  : '',
                              Icons.check_outlined,
                              1,false),
                          movimiento.agente!
                                      .toUpperCase()
                                      .replaceAll(" ", "") ==
                                  _prov.usuario!
                                      .toUpperCase()
                                      .replaceAll(" ", "")
                              ? Container(
                                  width: double.infinity,
                                  child: TextField(
                                    textAlign: TextAlign.left,
                                    keyboardType: TextInputType.text,
                                    controller: _solucionController,
                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.blue, width: 1.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.blue, width: 1.0),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                      labelText: movimiento.solucion!,
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      focusColor: Colors.blue,
                                      hoverColor: Colors.blue,
                                      fillColor: Colors.blue,
                                    ),
                                  ),
                                )
                              : Center(),
                          Divider(
                              height: 20,
                              color: CupertinoTheme.of(context)
                                  .primaryContrastingColor),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              _getRenglonDropDown(),
              _activeArticulos
                  ? Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Card(
                              child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: IconButton(
                                    icon: const Icon(Icons.send),
                                    tooltip: 'Scanear',
                                    onPressed: () {
                                      setState(() {});
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ),
                      ],
                    )
                  : Center(),
            ],
          ),
        ),
        //_servicioCardDetalle(),
      ),
    ]);
  }

  Widget _getRenglonDropDown() {
    return Padding(
      padding: const EdgeInsets.only(left: 5.0, right: 0.0),
      child: movimiento.nombre == "" ||
              movimiento.situacion.toString().contains("REASIGNAR AGENTE")
          ? Column(
              children: [

              ],
            )
          : Center(),
    );
  }

  _changeActivity() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
    //   Navigator.of(context).push(MaterialPageRoute(builder: (context) => CalendarioPage(detalle)));
  }

  Widget _servicioCardDetalle() {
    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 18.0, 10.0, 20.0),
      //  height: 100.0 * articulos.length  + 100,
      height: 100.0 ,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.brown,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0),
              bottomLeft: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0),
              topRight: Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(0.0, 0.0),
              blurRadius: 5.0,
            ),
          ]),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //_crearCodigoInput(),
            //SizedBox(height: 30.0),
          ],
        ),
      ),
    );
  }

  Widget _getRenglon(String value1, String? value2) {
    if (value2 == null || value2 == "null") {
      return Text("");
    }
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.30,
                  child: Text(
                    value1,
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Text(
                    value2,
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400),
                    maxLines: 2,
                    overflow: TextOverflow.fade,
                  ),
                ),
              ]),
            ],
          ),
        ),
        Divider()
      ],
    );
  }


  /*_getDetalle(id) async {
    if(id != ""){
      setState(() { _saving = true; });
      var res = await httpProv.spWebCitaDetalle(id);
      if (res.isNotEmpty) {
        if (res[0] != null) {
          setState(() {
            detalle = res[0];
            _saving = false; 
          });
        } else {
          setState(() { _saving = false; });
          print("Error en la Consulta");
        }
      }
      setState(() { _saving = false; });
    }else {
      DAToast(_scaffoldKey,"Sin Resultados");
    }

  }*/


  TableRow _getTitle() {
    return TableRow(
      children: <Widget>[
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 15.0, 8.0, 15.0),
              child: Text(
                "Partida",
                style: TextStyle(color: Colors.white, fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 15.0, 8.0, 15.0),
              child: Text(
                "Artículo",
                style: TextStyle(color: Colors.white, fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 15.0, 8.0, 15.0),
              child: Text(
                "Cant.",
                style: TextStyle(color: Colors.white, fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 15.0, 8.0, 15.0),
              child: Text(
                "Recibido",
                style: TextStyle(color: Colors.white, fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getFAB() {
    List<SpeedDialChild> listOptions = [];

    if (movimiento.nombre == "" || movimiento.situacion.toString().contains("REASIGNAR AGENTE")) {
      listOptions.add(SpeedDialChild(
          child: Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          backgroundColor: CupertinoTheme.of(context).primaryColor,
          onTap: () async {
            if (_opcionHoraSeleccionadaTecnico != null) {
            
            } else {
              DAToast(_scaffoldKey, "Llenar todos los campos");
            }
          },
          label: 'Confirmar',
          labelStyle: TextStyle(fontWeight: FontWeight.w500, color: Colors.white, fontSize: 16.0),
          labelBackgroundColor: CupertinoTheme.of(context).primaryColor)
          );
    }

  
    print(movimiento.agente);

    if (movimiento.nombre != "" && !movimiento.situacion.toString().contains("REASIGNAR AGENTE") && movimiento.agente!.toUpperCase().replaceAll(" ", "") == _prov.usuario!.toUpperCase().replaceAll(" ", "")) {
      listOptions.add(
        SpeedDialChild(
            child: Icon(
              Icons.check_circle,
              color: Colors.white,
            ),
            backgroundColor: CupertinoTheme.of(context).primaryColor,
            onTap: () {
              if (_solucionController.text != "") {
              
              } else {
                DAToast(_scaffoldKey, "Llenar el campo solución");
              }
            },
            label: 'Enviar solución',
            labelStyle: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 16.0),
            labelBackgroundColor: CupertinoTheme.of(context).primaryColor),
      );
    }

    if (movimiento.situacion.toString().contains("POR LIBERAR USUARIO") && movimiento.eMail!.toUpperCase().replaceAll(" ", "") == _prov.usuario!.toUpperCase().replaceAll(" ", "")) {
      listOptions.add(SpeedDialChild(
          child: Icon(
            Icons.check,
            color: Colors.white,
          ),
          backgroundColor: CupertinoTheme.of(context).primaryColor,
          onTap: () {
            _showChoiceDialogAprobar(context);
          },
          label: 'Aprobar',
          labelStyle: TextStyle(
              fontWeight: FontWeight.w500, color: Colors.white, fontSize: 16.0),
          labelBackgroundColor: CupertinoTheme.of(context).primaryColor));
      listOptions.add(SpeedDialChild(
          child: Icon(
            Icons.cancel,
            color: Colors.white,
          ),
          backgroundColor: CupertinoTheme.of(context).primaryColor,
          onTap: () {
            _showChoiceDialogRechazar(context);
          },
          label: 'Rechazar',
          labelStyle: TextStyle(
              fontWeight: FontWeight.w500, color: Colors.white, fontSize: 16.0),
          labelBackgroundColor: CupertinoTheme.of(context).primaryColor));
    }

    listOptions.add(SpeedDialChild(
        child: Icon(
          Icons.camera_alt,
          color: Colors.white,
        ),
        backgroundColor: CupertinoTheme.of(context).primaryColor,
        onTap: () {

        },
        label: 'Foto',
        labelStyle: TextStyle(
            fontWeight: FontWeight.w500, color: Colors.white, fontSize: 16.0),
        labelBackgroundColor: CupertinoTheme.of(context).primaryColor));

    listOptions.add(SpeedDialChild(
        child: Icon(
          Icons.image,
          color: Colors.white,
        ),
        backgroundColor: CupertinoTheme.of(context).primaryColor,
        onTap: () async {
        },
        label: 'Imagenes',
        labelStyle: TextStyle(
            fontWeight: FontWeight.w500, color: Colors.white, fontSize: 16.0),
        labelBackgroundColor: CupertinoTheme.of(context).primaryColor));

    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22),
      backgroundColor: CupertinoTheme.of(context).primaryColor,
      foregroundColor: Colors.white,
      visible: true,
      curve: Curves.bounceIn,
      children: listOptions,
    );
  }

  Future<void> _showChoiceDialogAprobar(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("¿ Seguro que quieres APROBAR el ticket ?"),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('Cancelar'),
                onPressed: () {
                  setState(() {});
                  Navigator.of(context, rootNavigator: true).pop('dialog');
                },
              ),
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text('Aceptar'),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop('dialog');
                  _aprobarSolucion();
                },
              ),
            ],
          );
        });
  }

  Future<void> _showChoiceDialogRechazar(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Motivo del Rechazo"),
            content: Padding(
                padding: const EdgeInsets.only(
                    top: 5.0, right: 5.0, left: 5.0, bottom: 5.0),
                child: TextField(
                  //autofocus: true,
                  controller: _descripcionController,

                  keyboardType: TextInputType.text,
                  cursorColor: Colors.green,
                  decoration: InputDecoration(
                    border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.orange)),
                    hintText: 'Descripción del Rechazo',
                    labelText: 'Descripción del Rechazo',
                    focusColor: Colors.green,
                    hoverColor: Colors.green,
                    fillColor: Colors.green,
                  ),
                )),
            actions: <Widget>[
              FlatButton(
                color: Colors.transparent,
                textColor: Colors.white,
                child: Text(
                  'Cerrar',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                onPressed: () {
                  setState(() {});
                  Navigator.of(context, rootNavigator: true).pop('dialog');
                },
              ),
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('Rechazar'),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop('dialog');
                
                },
              ),
            ],
          );
        });
  }

  _aprobarSolucion() async {
    setState(() {
      _saving = true;
      _aprobado = 1;
    });

 



  Future<void> _showMessgeDialogo(context, mensaje) {
    return showDialog(
      
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text(mensaje),
          //content: new Text(mensaje),
          actions: <Widget>[
            new TextButton(
              child: new Text("Ok"),
              onPressed: () {
                //_cerrar();
              },
            )
          ],
        );
      },
    );
  }




  Future<void> _showMessgeDialogoCorrectivo(context, mensaje) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: new Text(mensaje),
          // content: new Text(_responseAsignaTecnico),
          actions: <Widget>[
            new TextButton(
              child: new Text("Ok"),
              onPressed: () {
              //  _cerrar();
              },
            )
          ],
        );
      },
    );
  }

  _cerrar() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
    Navigator.pop(context);
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => ListJefePage(),
      ),
    );
  }


  Widget _crearComentarios() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextField(
        keyboardType: TextInputType.number,
        controller: comentariosController,
        decoration: InputDecoration(
          hintText: 'Comentarios',
          labelText: 'Comentarios',
        ),
      ),
    );
  }





  Widget _getImageButtons() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Row(
        children: [
          Expanded(
            flex: 5,
            child: Align(
              alignment: Alignment.center,
              child: MaterialButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                color: Colors.blue,
                textColor: Colors.white,
                child: Icon(
                  Icons.send_outlined,
                  size: 24,
                ),
                padding: EdgeInsets.all(12),
                shape: CircleBorder(),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Align(
              alignment: Alignment.center,
              child: MaterialButton(
                onPressed: () {
                  setState(() {
                    _imageFile = null;
                  });
                  Navigator.of(context).pop();
                },
                color: Colors.blue,
                textColor: Colors.white,
                child: Icon(
                  Icons.delete,
                  size: 24,
                ),
                padding: EdgeInsets.all(12),
                shape: CircleBorder(),
              ),
            ),
          )
        ],
      ),
    );
  }






  Widget _getDropdownVencimiento() {
    return Padding(
      padding:
          const EdgeInsets.only(top: 5.0, bottom: 5.0, left: 40, right: 40),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Center(
              child: DropdownButton<String>(
                isExpanded: true,
                value: _opcionHoraSeleccionadaVencimiento,
                hint: Text("Asigna Vencimiento",
                    style: new TextStyle(color: Color(0xFF006c3f)),
                    textAlign: TextAlign.center),
                onChanged: (String? newValue) {
                  setState(() {
                    _opcionHoraSeleccionadaVencimiento = newValue;
                  });
                },
                items: <String>[
                  //'Asigna Vencimiento',
                  '10 Dias',
                  //'48 horas',
                  //'1 Semana',
                  // '3 Semanas',
                  //'Por Política'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Center(
                        child: Text(value,
                            style: new TextStyle(color: Color(0xFF006c3f)),
                            textAlign: TextAlign.center)),
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

}
