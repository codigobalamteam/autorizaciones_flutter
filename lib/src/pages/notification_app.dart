import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationApp extends StatefulWidget {
  NotificationApp({Key? key}) : super(key: key);

  @override
  _NotificationAppState createState() => _NotificationAppState();
}

class _NotificationAppState extends State<NotificationApp> {
  late FlutterLocalNotificationsPlugin localNotification;
  @override
  void initState() {
    super.initState();
    var androidInitialize =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iosInitialize = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: androidInitialize, iOS: iosInitialize);
    localNotification = new FlutterLocalNotificationsPlugin();
    localNotification.initialize(initializationSettings);
  }

  Future _showNotifications() async {
    var androidDetails = new AndroidNotificationDetails(
        'channelId', 'Local Notification', 'This is the description of message',
        importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationsDetails =
        new NotificationDetails(android: androidDetails, iOS: iosDetails);
    await localNotification.show(0, 'Notificacion title', 'Notificacion body',
        generalNotificationsDetails);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Titulo'),
      ),
      body: Center(
        child: Text("ALGO AQUI Y ALLA "),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showNotifications();
        },
        child: Icon(Icons.notifications),
      ),
    );
  }
}
