import 'package:flutter/cupertino.dart';
import 'package:qr_bar_code_flutter/src/config_app/login_page.dart';
import 'package:qr_bar_code_flutter/src/pages/list_jefe_page.dart';
import 'package:qr_bar_code_flutter/src/pages/notification_app.dart';
import 'package:qr_bar_code_flutter/src/pages/received_notification.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    //'/'     : (BuildContext context) => HomePage(),
    'login': (BuildContext context) => LoginPage(),
    'notification': (BuildContext context) => NotificationApp(),
    'jefes': (BuildContext context) => ListJefePage(),
    'received': (BuildContext context) => ReceivedNotifications(),
    //'recepcion': (BuildContext context) => RecepcionPage(),
  };
}
