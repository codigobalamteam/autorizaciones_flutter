import 'package:intl/intl.dart';

bool isNumeric(String s) {
  if (s.isEmpty) return false;
  final n = num.tryParse(s);
  return (n == null) ? false : true;
}


String formatCurrency(double amount) {
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  String currency = oCcy.format(amount);
  return '\$ ${currency}';
}

String formatCurrencyString(String amount) {
 try{
  double amountDouble = double.parse(amount);
  final oCcy = new NumberFormat("#,###.##", "en_US");
  String currency = oCcy.format(amountDouble);
  return '\$ ${currency}';
 }catch(e){
   return '';
 }
}

String formatDate(DateTime amount) {
  var formatDate = DateFormat('MM/dd/yyyy');
  String date = formatDate.format(amount);
  return date;
}

String formatDate2(dynamic amount) {
  if(amount == ""){
      return "";
  }
  var formatDate = DateFormat('dd/MM/yyyy');
  String date = formatDate.format(amount);
  return date;
}

String formatTime(DateTime amount) {
  var formatDate = DateFormat('HH:mm');
  String time = formatDate.format(amount);
  return time;
}

String formatDateTime(DateTime amount) {
  var formatDate = DateFormat('MM/dd/yyyy HH:mm');
  String date = formatDate.format(amount);
  return date;
}


