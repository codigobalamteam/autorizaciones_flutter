// To parse this JSON data, do
//
//     final detalle = detalleFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Detalle> detalleFromJson(String str) => List<Detalle>.from(json.decode(str).map((x) => Detalle.fromJson(x)));

String detalleToJson(List<Detalle> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Detalle {
    Detalle({
        @required this.concepto,
        @required this.descripcion,
        @required this.cantidad,
        @required this.precio,
        @required this.subTotal,
    });

    String? concepto;
    String? descripcion;
    dynamic? cantidad;
    dynamic? precio;
    dynamic? subTotal;

    factory Detalle.fromJson(Map<String, dynamic> json) => Detalle(
        concepto: json["Concepto"] == null ? "" : json["Concepto"],
        descripcion: json["Descripcion"] == null ? "" : json["Descripcion"],
        cantidad: json["Cantidad"] == null ? "" : json["Cantidad"].toDouble(),
        precio: json["Precio"] == null ? "" : json["Precio"].toDouble(),
        subTotal: json["SubTotal"] == null ? "" : json["SubTotal"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "Concepto": concepto == null ? null : concepto,
        "Descripcion": descripcion == null ? null : descripcion,
        "Cantidad": cantidad == null ? null : cantidad,
        "Precio": precio == null ? null : precio,
        "SubTotal": subTotal == null ? null : subTotal,
    };
}
