// To parse this JSON data, do
//
//     final empresa = empresaFromJson(jsonString);

import 'dart:convert';

List<OkRef> okrefFromJson(String str) =>
    List<OkRef>.from(json.decode(str).map((x) => OkRef.fromJson(x)));

String okrefToJson(List<OkRef> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OkRef {
  OkRef({this.rol, this.ok, this.okRef, this.soporteId});

  String? rol;
  String? ok;
  String? okRef;
  String? soporteId;
  factory OkRef.fromJson(Map<String, dynamic> json) => OkRef(
        rol: json["Rol"] == null ? "" : json["Rol"],
        ok: json["Ok"] == null ? "" : json["Ok"],
        okRef: json["OkRef"] == null ? "" : json["OkRef"],
        soporteId: json["SoporteID"] == null ? "" : json["SoporteID"],
      );

  Map<String, dynamic> toJson() => {
        "Rol": rol == null ? null : rol,
        "Ok": ok == null ? null : ok,
        "OkRef": okRef == null ? null : okRef,
        "SoporteID": soporteId == null ? null : soporteId,
      };
}
