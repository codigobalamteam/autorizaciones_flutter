// To parse this JSON data, do
//
//     final rol = rolFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Rol> rolFromJson(String str) => List<Rol>.from(json.decode(str).map((x) => Rol.fromJson(x)));

String rolToJson(List<Rol> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

String rolModelToJsonString(Rol data) => json.encode(data.toJson());

Rol rolModelFromJsonString(String str) => Rol.fromJson(json.decode(str));

class Rol {
    Rol({
        @required this.appAutCxp,
        @required this.appAutCompra,
        @required this.appAutGasto,
        @required this.appAutCxc,
        @required this.grupoTrabajo,
    });

    dynamic? appAutCxp;
    dynamic? appAutCompra;
    dynamic? appAutGasto;
    dynamic? appAutCxc;
    String? grupoTrabajo;

    factory Rol.fromJson(Map<String, dynamic> json) => Rol(
        appAutCxp: json["AppAutCxp"] == null ? false : json["AppAutCxp"],
        appAutCompra: json["AppAutCompra"] == null ? false : json["AppAutCompra"],
        appAutGasto: json["AppAutGasto"] == null ? false : json["AppAutGasto"],
        appAutCxc: json["AppAutCxc"] == null ? false : json["AppAutCxc"],
        grupoTrabajo: json["GrupoTrabajo"] == null ? "" : json["GrupoTrabajo"],
    );

    Map<String, dynamic> toJson() => {
        "AppAutCxp": appAutCxp == null ? false : appAutCxp,
        "AppAutCompra": appAutCompra == null ? false : appAutCompra,
        "AppAutGasto": appAutGasto == null ? false : appAutGasto,
        "AppAutCxc": appAutCxc == null ? false : appAutCxc,
        "GrupoTrabajo": grupoTrabajo == null ? null : grupoTrabajo,
    };
}



