// To parse this JSON data, do
//
//     final empresa = empresaFromJson(jsonString);

import 'dart:convert';

List<Clasificacion> clasificacionFromJson(String str) =>
    List<Clasificacion>.from(
        json.decode(str).map((x) => Clasificacion.fromJson(x)));

String clasificacionToJson(List<Clasificacion> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Clasificacion {
  Clasificacion({this.clase});

  String? clase;

  factory Clasificacion.fromJson(Map<String, dynamic> json) => Clasificacion(
        clase: json["Clase"] == null ? "" : json["Clase"],
      );

  Map<String, dynamic> toJson() => {
        "Clase": clase == null ? null : clase,
      };
}
