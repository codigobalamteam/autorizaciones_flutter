// To parse this JSON data, do
//
//     final empresa = empresaFromJson(jsonString);

import 'dart:convert';

List<Empresa> empresaFromJson(String str) =>
    List<Empresa>.from(json.decode(str).map((x) => Empresa.fromJson(x)));

String empresaToJson(List<Empresa> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Empresa {
  Empresa(
      {this.empresa,
      this.sucursal,
      this.bd,
      this.modulo,
      this.id,
      this.usuario,
      this.email,
      this.nombre});

  String? empresa;
  String? sucursal;
  String? bd;
  String? modulo;
  String? id;
  String? usuario;
  String? email;
  String? nombre;

  factory Empresa.fromJson(Map<String, dynamic> json) => Empresa(
        empresa: json["Empresa"] == null ? "" : json["Empresa"],
        sucursal: json["Sucursal"] == null ? "" : json["Sucursal"],
        bd: json["bd"] == null ? "" : json["bd"],
        modulo: json["modulo"] == null ? "" : json["modulo"],
        id: json["id"] == null ? "" : json["id"],
        usuario: json["usuario"] == null ? "" : json["usuario"],
        email: json["email"] == null ? "" : json["email"],
        nombre: json["Nombre"] == null ? "" : json["Nombre"],
      );

  Map<String, dynamic> toJson() => {
        "Empresa": empresa == null ? null : empresa,
        "Sucursal": sucursal == null ? null : sucursal,
        "bd": bd == null ? null : bd,
        "modulo": modulo == null ? null : modulo,
        "id": id == null ? null : id,
        "usuario": usuario == null ? null : usuario,
        "email": email == null ? null : email,
        "Nombre": nombre == null ? null : nombre,
      };
}
