// To parse this JSON data, do
//
//     final uen = uenFromJson(jsonString);

import 'dart:convert';

List<Uen> uenFromJson(String str) => List<Uen>.from(json.decode(str).map((x) => Uen.fromJson(x)));

String uenToJson(List<Uen> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Uen {
    Uen({
        this.uen,
        this.nombre,
    });

    String? uen;
    String? nombre;

    factory Uen.fromJson(Map<String, dynamic> json) => Uen(
        uen: json["UEN"] == null ? null : json["UEN"],
        nombre: json["Nombre"] == null ? null : json["Nombre"],
    );

    Map<String, dynamic> toJson() => {
        "UEN": uen == null ? null : uen,
        "Nombre": nombre == null ? null : nombre,
    };
}
