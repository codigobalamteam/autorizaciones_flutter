// To parse this JSON data, do
//
//     final empresa = empresaFromJson(jsonString);

import 'dart:convert';

List<Area> areaFromJson(String str) =>
    List<Area>.from(json.decode(str).map((x) => Area.fromJson(x)));

String areaToJson(List<Area> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Area {
  Area({this.area, this.email, this.empresa});

  String? area;
  String? email;
  String? empresa;

  factory Area.fromJson(Map<String, dynamic> json) => Area(
        area: json["Area"] == null ? "" : json["Area"],
        email: json["Email"] == null ? "" : json["Email"],
        empresa: json["Empresa"] == null ? "" : json["Empresa"],
      );

  Map<String, dynamic> toJson() => {
        "Area": area == null ? null : area,
        "Email": email == null ? null : email,
        "Empresa": empresa == null ? null : empresa,
      };
}
