import 'dart:convert';

List<Movimiento> movimientoFromJson(String str) => List<Movimiento>.from(json.decode(str).map((x) => Movimiento.fromJson(x)));

String movimientoToJson(List<Movimiento> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Movimiento {
    Movimiento({
      this.dateBase,
      this.modulo,
      this.id,
      this.empresa,
      this.empresaNombre,
      this.mov,
      this.movId,
      this.fechaEmision,
      this.observaciones,
      this.moneda,
      this.tipoCambio,
      this.usuario,
      this.nombre,
      this.email,
      this.estatus,
      this.situacion,
      this.situacionFecha,
      this.situacionUsuario,
      this.proveedor,
      this.provNombre,
      this.clase,
      this.subClase,
      this.sucursal,
      this.sucursalNombre,
      this.referencia,
      this.subTotal,
      this.grupoTrabajo,
      this.uen,
      this.uenNombre,
    });

    String? dateBase;
    String? modulo;
    int? id;
    String? empresa;
    String? empresaNombre;
    String? mov;
    String? movId;
    DateTime? fechaEmision;
    String? observaciones;
    String? moneda;
    dynamic? tipoCambio;
    String? usuario;
    String? nombre;
    String? email;
    String? estatus;
    String? situacion;
    dynamic situacionFecha;
    dynamic situacionUsuario;
    String? proveedor;
    String? provNombre;
    dynamic clase;
    dynamic subClase;
    int? sucursal;
    String? sucursalNombre;
    dynamic referencia;
    dynamic? subTotal;
    String? grupoTrabajo;
    dynamic? uen;
    String? uenNombre;


    factory Movimiento.fromJson(Map<String, dynamic> json) => Movimiento(
        dateBase: json["DateBase"] == null ? "" : json["DateBase"],
        modulo: json["Modulo"] == null ? "" : json["Modulo"],
        id: json["ID"] == null ? "" : json["ID"],
        empresa: json["Empresa"] == null ? "" : json["Empresa"],
        empresaNombre: json["EmpresaNombre"] == null ? "" : json["EmpresaNombre"],
        mov: json["Mov"] == null ? "" : json["Mov"],
        movId: json["MovID"] == null ? "" : json["MovID"],
        fechaEmision: json["FechaEmision"] == null ? null : DateTime.parse(json["FechaEmision"]),
        observaciones: json["Observaciones"] == null ? "" : json["Observaciones"],
        moneda: json["Moneda"] == null ? "" : json["Moneda"],
        tipoCambio: json["TipoCambio"] == null ? "" : json["TipoCambio"],
        usuario: json["Usuario"] == null ? "" : json["Usuario"],
        nombre: json["Nombre"] == null ? "" : json["Nombre"],
        email: json["Email"] == null ? "" : json["Email"],
        estatus: json["Estatus"] == null ? "" : json["Estatus"],
        situacion: json["Situacion"] == null ? "" : json["Situacion"],
        situacionFecha:json["SituacionFecha"] == null ? "" : json["SituacionFecha"],
        situacionUsuario:json["SituacionUsuario"] == null ? "" : json["SituacionUsuario"],
        proveedor: json["Proveedor"] == null ? "" : json["Proveedor"],
        provNombre: json["ProvNombre"] == null ? "" : json["ProvNombre"],
        clase: json["Clase"] == null ? "" :json["Clase"],
        subClase: json["SubClase"] == null ? "" : json["SubClase"],
        sucursal: json["Sucursal"] == null ? null : json["Sucursal"],
        sucursalNombre: json["SucursalNombre"] == null ? null : json["SucursalNombre"],
        referencia: json["Referencia"] == null ? "" : json["Referencia"],
        subTotal: json["SubTotal"] == null ? "" : json["SubTotal"],
        grupoTrabajo: json["GrupoTrabajo"] == null ? "" : json["GrupoTrabajo"],
        uen: json["UEN"] == null ? "" : json["UEN"],
        uenNombre: json["UENombre"] == null ? "" : json["UENombre"],
    );

    Map<String, dynamic> toJson() => {
        "DateBase": dateBase == null ? null : dateBase,
        "Modulo": modulo == null ? null : modulo,
        "ID": id == null ? null : id,
        "Empresa": empresa == null ? null : empresa,
        "EmpresaNombre": empresaNombre == null ? null : empresaNombre,
        "Mov": mov == null ? null : mov,
        "MovID": movId == null ? null : movId,
        "FechaEmision": fechaEmision == null ? null : fechaEmision!.toIso8601String(),
        "Observaciones": observaciones == null ? null : observaciones,
        "Moneda": moneda == null ? null : moneda,
        "TipoCambio": tipoCambio == null ? null : tipoCambio,
        "Usuario": usuario == null ? null : usuario,
        "Nombre": nombre == null ? null : nombre,
        "Email": email == null ? null : email,
        "Estatus": estatus == null ? null : estatus,
        "Situacion": situacion == null ? null : situacion,
        "SituacionFecha": situacionFecha,
        "SituacionUsuario": situacionUsuario,
        "Proveedor": proveedor == null ? null : proveedor,
        "ProvNombre": provNombre == null ? null : provNombre,
        "Clase": clase,
        "SubClase": subClase,
        "Sucursal": sucursal == null ? null : sucursal,
        "SucursalNombre": sucursalNombre == null ? null : sucursalNombre,
        "Referencia": referencia,
        "SubTotal": subTotal == null ? null : subTotal,
        "GrupoTrabajo": grupoTrabajo == null ? null : grupoTrabajo,
    };
}
