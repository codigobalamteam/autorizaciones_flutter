// To parse this JSON data, do
//
//     final soporteCliente = soporteClienteFromJson(jsonString);

import 'dart:convert';

List<SoporteCliente> soporteClienteFromJson(String str) =>
    List<SoporteCliente>.from(
        json.decode(str).map((x) => SoporteCliente.fromJson(x)));

String soporteClienteToJson(List<SoporteCliente> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SoporteCliente {
  SoporteCliente(
      {this.id,
      this.empresa,
      this.mov,
      this.movId,
      this.fechaEmision,
      this.situacion,
      this.situacionFecha,
      this.referencia,
      this.agente,
      this.nombre,
      this.estado,
      this.eMail,
      this.fechaInicio,
      this.vencimiento,
      this.titulo,
      this.problema,
      this.solucion,
      this.fechaRegistro,
      this.sucursal,
      this.sucNombre,
      this.estatus,
      this.area,
      this.liberarUsuario,
      this.email,
      this.importe,
      this.usuarioNombre});

  String? id;
  String? empresa;
  String? mov;
  String? movId;
  dynamic? fechaEmision;
  String? situacion;
  DateTime? situacionFecha;
  dynamic? referencia;
  String? agente;
  String? nombre;
  String? estado;
  String? eMail;
  DateTime? fechaInicio;
  dynamic? vencimiento;
  String? titulo;
  String? problema;
  dynamic? solucion;
  DateTime? fechaRegistro;
  String? sucursal;
  String? sucNombre;
  String? estatus;
  dynamic? area;
  String? liberarUsuario;
  String? email;
  dynamic? importe;
  String? usuarioNombre;

  factory SoporteCliente.fromJson(Map<String, dynamic> json) => SoporteCliente(
        id: json["ID"] == null ? "" : json["ID"],
        empresa: json["Empresa"] == null ? "" : json["Empresa"],
        mov: json["Mov"] == null ? "" : json["Mov"],
        movId: json["MovID"] == null ? "" : json["MovID"],
        fechaEmision: json["FechaEmision"] == null
            ? ""
            : DateTime.parse(json["FechaEmision"]),
        situacion: json["Situacion"] == null ? "" : json["Situacion"],
        situacionFecha: json["SituacionFecha"] == null
            ? null
            : DateTime.parse(json["SituacionFecha"]),
        referencia: json["Referencia"] == null ? "" : json["Referencia"],
        agente: json["Agente"] == null ? "" : json["Agente"],
        nombre: json["Nombre"] == null ? "" : json["Nombre"],
        estado: json["Estado"] == null ? "" : json["Estado"],
        eMail: json["eMail"] == null ? "" : json["eMail"],
        fechaInicio: json["FechaInicio"] == null
            ? null
            : DateTime.parse(json["FechaInicio"]),
        vencimiento: json["Vencimiento"] == null
            ? ""
            : DateTime.parse(json["Vencimiento"]),
        titulo: json["Titulo"] == null ? "" : json["Titulo"],
        problema: json["Problema"] == null ? "" : json["Problema"],
        solucion: json["Solucion"] == null
            ? "En espera de Solución"
            : json["Solucion"],
        fechaRegistro: json["FechaRegistro"] == null
            ? null
            : DateTime.parse(json["FechaRegistro"]),
        sucursal: json["Sucursal"] == null ? "" : json["Sucursal"],
        sucNombre: json["SucNombre"] == null ? "" : json["SucNombre"],
        estatus: json["Estatus"] == null ? "" : json["Estatus"],
        area: json["Area"] == null ? "" : json["Area"],
        liberarUsuario:
            json["LiberarUsuario"] == null ? "" : json["LiberarUsuario"],
        email: json["Email"] == null ? "" : json["Email"],
        importe: json["Importe"] == null ? "" : json["Importe"],
        usuarioNombre:
            json["UsuarioNombre"] == null ? "" : json["UsuarioNombre"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id == null ? null : id,
        "Empresa": empresa == null ? null : empresa,
        "Mov": mov == null ? null : mov,
        "MovID": movId == null ? null : movId,
        "FechaEmision":
            fechaEmision == null ? null : fechaEmision!.toIso8601String(),
        "Situacion": situacion == null ? null : situacion,
        "SituacionFecha":
            situacionFecha == null ? null : situacionFecha!.toIso8601String(),
        "Referencia": referencia,
        "Agente": agente == null ? null : agente,
        "Nombre": nombre == null ? null : nombre,
        "Estado": estado == null ? null : estado,
        "eMail": eMail == null ? null : eMail,
        "FechaInicio":
            fechaInicio == null ? null : fechaInicio!.toIso8601String(),
        "Vencimiento":
            vencimiento == null ? null : vencimiento!.toIso8601String(),
        "Titulo": titulo == null ? null : titulo,
        "Problema": problema == null ? null : problema,
        "Solucion": solucion,
        "FechaRegistro":
            fechaRegistro == null ? null : fechaRegistro!.toIso8601String(),
        "Sucursal": sucursal == null ? null : sucursal,
        "SucNombre": sucNombre == null ? null : sucNombre,
        "Estatus": estatus == null ? null : estatus,
        "Area": area,
        "LiberarUsuario": liberarUsuario == null ? null : liberarUsuario,
        "Email": email == null ? null : email,
        "UsuarioNombre": usuarioNombre == null ? null : usuarioNombre,
      };
}
