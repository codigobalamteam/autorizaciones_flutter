// To parse this JSON data, do
//
//     final ticketsPendientes = ticketsPendientesFromJson(jsonString);

import 'dart:convert';

List<TicketsPendientes> ticketsPendientesFromJson(String str) =>
    List<TicketsPendientes>.from(
        json.decode(str).map((x) => TicketsPendientes.fromJson(x)));

String ticketsPendientesToJson(List<TicketsPendientes> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TicketsPendientes {
  TicketsPendientes({
    this.id,
    this.empresa,
    this.mov,
    this.movId,
    this.fechaEmision,
    this.situacion,
    this.situacionFecha,
    this.referencia,
    this.agente,
    this.nombre,
    this.estado,
    this.eMail,
    this.fechaInicio,
    this.vencimiento,
    this.titulo,
    this.problema,
    this.solucion,
    this.fechaRegistro,
    this.sucursal,
    this.sucNombre,
    this.estatus,
    this.area,
    this.motivoRechazo,
    this.usuarioNombre,
    this.uen,
    this.nombreUEN,
    this.importe,
  });

  String? id;
  String? empresa;
  String? mov;
  String? movId;
  dynamic? fechaEmision;
  String? situacion;
  DateTime? situacionFecha;
  dynamic? referencia;
  String? agente;
  String? nombre;
  String? estado;
  String? eMail;
  DateTime? fechaInicio;
  dynamic? vencimiento;
  String? titulo;
  String? problema;
  dynamic? solucion;
  DateTime? fechaRegistro;
  String? sucursal;
  String? sucNombre;
  String? estatus;
  dynamic? area;
  dynamic? motivoRechazo;
  String? usuarioNombre;
  dynamic? uen;
  String? nombreUEN;
  String? importe;

  factory TicketsPendientes.fromJson(Map<String, dynamic> json) =>
      TicketsPendientes(
        id: json["ID"] == null ? "" : json["ID"],
        empresa: json["Empresa"] == null ? "" : json["Empresa"],
        mov: json["Mov"] == null ? "" : json["Mov"],
        movId: json["MovID"] == null ? "" : json["MovID"],
        fechaEmision: json["FechaEmision"] == null ? "" : DateTime.parse(json["FechaEmision"]),
        situacion: json["Situacion"] == null ? "" : json["Situacion"],
        situacionFecha: json["SituacionFecha"] == null ? null : DateTime.parse(json["SituacionFecha"]),
        referencia: json["Referencia"] == null ? "" : json["Referencia"],
        agente: json["Agente"] == null ? "" : json["Agente"],
        nombre: json["Nombre"] == null || json["Nombre"] == "null" ? "" : json["Nombre"],
        estado: json["Estado"] == null ? "" : json["Estado"],
        eMail: json["eMail"] == null ? "" : json["eMail"],
        fechaInicio: json["FechaInicio"] == null ? null : DateTime.parse(json["FechaInicio"]),
        vencimiento: json["Vencimiento"] == null ? "" : DateTime.parse(json["Vencimiento"]),
        titulo: json["Titulo"] == null ? "" : json["Titulo"],
        problema: json["Problema"] == null ? "" : json["Problema"],
        solucion: json["Solucion"] == null ? "" : json["Solucion"],
        fechaRegistro: json["FechaRegistro"] == null ? null : DateTime.parse(json["FechaRegistro"]),
        sucursal: json["Sucursal"] == null ? "" : json["Sucursal"],
        sucNombre: json["SucNombre"] == null ? "" : json["SucNombre"],
        estatus: json["Estatus"] == null ? "" : json["Estatus"],
        area: json["Area"] == null ? "" : json["Area"],
        motivoRechazo: json["MotivoRechazo"] == null ? "" : json["MotivoRechazo"],
        usuarioNombre: json["UsuarioNombre"] == null ? "" : json["UsuarioNombre"],
        uen: json["UEN"] == null ? "" : json["UEN"],
        nombreUEN: json["NombreUEN"] == null ? "" : json["NombreUEN"],
        importe: json["Importe"] == null ? "" : json["Importe"],
      );


  Map<String, dynamic> toJson() => {
        "ID": id == null ? null : id,
        "Empresa": empresa == null ? null : empresa,
        "Mov": mov == null ? null : mov,
        "MovID": movId == null ? null : movId,
        "FechaEmision": fechaEmision == null ? null : fechaEmision!.toIso8601String(),
        "Situacion": situacion == null ? "" : situacion,
        "SituacionFecha": situacionFecha == null ? null : situacionFecha!.toIso8601String(),
        "Referencia": referencia,
        "Agente": agente == null ? null : agente,
        "Nombre": nombre == null ? null : nombre,
        "Estado": estado == null ? null : estado,
        "eMail": eMail == null ? null : eMail,
        "FechaInicio": fechaInicio == null ? null : fechaInicio!.toIso8601String(),
        "Vencimiento": vencimiento == null ? null : vencimiento!.toIso8601String(),
        "Titulo": titulo == null ? null : titulo,
        "Problema": problema == null ? null : problema,
        "Solucion": solucion,
        "FechaRegistro": fechaRegistro == null ? null : fechaRegistro!.toIso8601String(),
        "Sucursal": sucursal == null ? null : sucursal,
        "SucNombre": sucNombre == null ? null : sucNombre,
        "Estatus": estatus == null ? null : estatus,
        "Area": area,
        "MotivoRechazo": motivoRechazo,
        "UsuarioNombre": usuarioNombre == null ? null : usuarioNombre,
      };
}
